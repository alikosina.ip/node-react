FROM node:14-alpine

WORKDIR /var/www/app

RUN npm install -g npm@latest

COPY package.json ./

COPY yarn.lock ./

RUN yarn install --frozen-lockfile

COPY . .

EXPOSE 3000

CMD ["npm", "run", "start"]