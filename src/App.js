import { useEffect, useState } from 'react'
import axios from 'axios'
import './App.css';

function App() {

  const [products, setProducts] = useState([])

  const getProducts = async () => {
    const response = await axios.get('http://localhost:5000');
    setProducts(response.data)
  }

  useEffect(() => {
    getProducts()
  }, [])


  console.log('products = ', products);


  return (
    <div>
    <h1>Hello world</h1>
    <div>
     {
       products.map( (item) => (
        <div key={item.name}>
          {item.name}
        </div>
        ) )
     }
    </div>
    </div>
  );
}

export default App;
