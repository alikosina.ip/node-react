build:
	docker build -f Dockerfile . -t node-react
	docker run --rm  -it -v ${PWD}:/var/www/app --name my-frontend --network my-network -p 3000:3000 node-react